package com.javagda23.zad1;

public class Corka implements ICzlonekRodziny{
    private String imie;

    public Corka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Jestem córka. -" + imie);
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
