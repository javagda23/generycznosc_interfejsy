package com.javagda23.zad1;

public class Syn implements ICzlonekRodziny {
    private String imie;

    public Syn(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Czy jest tu jakiś cwaniak? -" + imie);
    }

    @Override
    public boolean jestDorosły() {
        return false;
    }
}
