package com.javagda23.zad1;

public class Ojciec implements ICzlonekRodziny {
    private String imie;

    public Ojciec(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("I am Your father. -" + imie);
    }

    public void nakrzyczNaSyna(){
        System.out.println("oj synku synku!");
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
