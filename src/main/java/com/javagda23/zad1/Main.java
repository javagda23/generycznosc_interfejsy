package com.javagda23.zad1;

public class Main {
    public static void main(String[] args) {
        ICzlonekRodziny[] czlonekRodzinies = new ICzlonekRodziny[5];
        czlonekRodzinies[0] = new Ojciec("Arek");
        czlonekRodzinies[1] = new Syn("Wojtek");
        czlonekRodzinies[2] = new Matka("Ania");
        czlonekRodzinies[3] = new Corka("Kasia");
        czlonekRodzinies[4] = new Ciocia();

//        Ojciec ojciec = null;
        for (ICzlonekRodziny czlonekRodziny : czlonekRodzinies) {
            if (czlonekRodziny instanceof Ojciec) {
                Ojciec ojciec = (Ojciec) czlonekRodziny;
                // rzutowanie
                ojciec.nakrzyczNaSyna();
            }
            czlonekRodziny.przedstawSie();
            System.out.println("Dorosły: " + czlonekRodziny.jestDorosły());
            System.out.println(); // < - dopisałem jeden 'enter' żeby oddzielić od siebie kolejne wypisania
        }

//        if(ojciec != null){
//            ojciec.nakrzyczNaSyna();
//        }

    }
}
