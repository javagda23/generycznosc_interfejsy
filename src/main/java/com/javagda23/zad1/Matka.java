package com.javagda23.zad1;

public class Matka implements ICzlonekRodziny {
    private String imie;

    public Matka(String imie) {
        this.imie = imie;
    }

    @Override
    public void przedstawSie() {
        System.out.println("Jestem mamusią! -" + imie);
    }

    @Override
    public boolean jestDorosły() {
        return true;
    }
}
