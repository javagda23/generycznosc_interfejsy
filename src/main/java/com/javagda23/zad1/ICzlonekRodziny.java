package com.javagda23.zad1;

public interface ICzlonekRodziny {
    //
    default void przedstawSie() {
        System.out.println("I am just a simple family member.");
    }

    boolean jestDorosły();
}
