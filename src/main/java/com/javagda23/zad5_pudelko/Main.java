package com.javagda23.zad5_pudelko;

import com.javagda23.przyklad_generycznosc.Person;

public class Main {
    public static void main(String[] args) {
        Pudelko<String> pudelkoStr = new Pudelko<>("ja");
        Pudelko<Integer> pudelkoInt = new Pudelko<>(23);

        Pudelko<Person> personPudelko = new Pudelko<>();

        pudelkoInt.whatsInTheBox();
        pudelkoStr.whatsInTheBox();
        personPudelko.whatsInTheBox();
    }
}
