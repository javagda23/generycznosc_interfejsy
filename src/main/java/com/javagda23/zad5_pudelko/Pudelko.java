package com.javagda23.zad5_pudelko;

public class Pudelko<T> {
    private T zawartosc;

    public Pudelko(T zawartosc) {
        this.zawartosc = zawartosc;
    }

    public Pudelko() {
    }

    public T getZawartosc() {
        return zawartosc;
    }

    public void whatsInTheBox() {
        if (zawartosc == null) {
            System.out.println("Pudełko jest puste!");
        } else {
            System.out.println("W pudełku jest: " + zawartosc);
        }
    }
}
