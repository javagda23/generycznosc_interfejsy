package com.javagda23.generycznosc_para;

import com.javagda23.przyklad_generycznosc.Person;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Para<Person> ludzi = new Para<>(
                new Person("Ania", 23),
                new Person("Mirek", 20));

        Para<Integer> teksty = new Para<>(2, 5);

        Para<Person>[] pary = new Para[]{
            new Para(new Person("a", 1), new Person("b",2)),
            new Para(new Person("x", 3), new Person("i",4)),
            new Para(new Person("g", 5), new Person("y",6)),
            new Para(new Person("n", 7), new Person("t",8)),
            new Para(new Person("z", 9), new Person("f",10))
        };

        System.out.println(Arrays.toString(pary));

//        pary[0].
    }
}
