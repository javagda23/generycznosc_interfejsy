package com.javagda23.przyklad;

public interface IClosable {
    public static final int STALA_A = 5;

    public abstract void close();

    public default void metoda(){
        System.out.println("Riki tiki");
    }
}
