package com.javagda23.przyklad;

public class JakasKlasa implements IInnyInterfejs, IClosable {

    @Override
    public void close() {

    }

    @Override
    public void metoda() {
        IClosable.super.metoda();
        IInnyInterfejs.super.metoda();
    }

    // SOLID
    // I - Interface segregation
}
