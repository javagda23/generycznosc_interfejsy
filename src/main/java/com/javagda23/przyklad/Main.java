package com.javagda23.przyklad;

public class Main {
    public static void main(String[] args) {
        IClosable[] zamykalne = new IClosable[2];
        zamykalne[0] = new Drzwi();
        zamykalne[1] = new Okno();

        for (IClosable iClosable : zamykalne) {
            iClosable.close();
        }


        JakasKlasa klasa = new JakasKlasa();
        klasa.metoda();
    }
}
