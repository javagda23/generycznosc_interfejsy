package com.javagda23.comparable_comparator_przyklad;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();

        // 1.
//        PersonComparator comparator = new PersonComparator();
//        Collections.sort(personList, comparator);

        // 2.
//        Collections.sort(personList, new PersonComparator());

        // 3. Stworzenie klasy anonimowej
        Collections.sort(personList, new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                if (o1.getWiek() > o2.getWiek()) {
                    return -1;
                } else if (o1.getWiek() < o2.getWiek()) {
                    return 1;
                }
                return 0;
            }
        });

    }
}
