package com.javagda23.comparable_comparator_przyklad;

import java.util.Comparator;

public class PersonComparator implements Comparator<Person> {
    @Override
    public int compare(Person o1, Person o2) {
        if (o1.getWiek() > o2.getWiek()) {
            return -1;
        } else if (o1.getWiek() < o2.getWiek()) {
            return 1;
        }
        return 0;
    }
}
