package com.javagda23.zad4_citizen;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Citizen[] citizens = new Citizen[10];
        citizens[0] = new King("Marian");
        citizens[1] = new Soldier("Łukasz");
        citizens[2] = new Soldier("Michał");
        citizens[3] = new Soldier("Wacław");
        citizens[4] = new Soldier("Olek");
        citizens[5] = new Townsman("Franek");
        citizens[6] = new Townsman("Ania");
        citizens[7] = new Townsman("Wojtek");
        citizens[8] = new Peasant("Rafał");
        citizens[9] = new Peasant("Ignacy");

        // Pamiętajmy o nadpisaniu metody toString w klasie Citizen.
//        System.out.println(citizens[5]);

        Town miasteczko = new Town(citizens);

        System.out.println("Zdolnych do głosowania: " + miasteczko.howManyCanVote());
        System.out.println("Zdolni do głosowania: " + Arrays.toString(miasteczko.whoCanVote()));
    }
}
