package com.javagda23.zad4_citizen;

public class Townsman extends Citizen {
    public Townsman(String imie) {
        super(imie);
    }

    @Override
    boolean canVote() {
        return true;
    }
}
