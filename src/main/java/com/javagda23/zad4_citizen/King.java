package com.javagda23.zad4_citizen;

public class King extends Citizen {
    public King(String imie) {
        super(imie);
    }

    @Override
    boolean canVote() {
        return false;
    }

}
