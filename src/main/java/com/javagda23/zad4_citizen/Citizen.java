package com.javagda23.zad4_citizen;

//Obywatel
public abstract class Citizen {
    private String imie;

    public Citizen(String imie) {
        this.imie = imie;
    }

    abstract boolean canVote();

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "imie='" + imie + '\'' +
                '}';
    }
}
