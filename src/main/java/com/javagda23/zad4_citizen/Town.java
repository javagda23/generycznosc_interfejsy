package com.javagda23.zad4_citizen;

public class Town {
    private Citizen[] obywatele;

    public Town(Citizen[] obywatele) {
        this.obywatele = obywatele;
    }

    // ilu obywateli może głosować
    public int howManyCanVote() {
        int licznik = 0;
        for (Citizen citizen : obywatele) {
            if (citizen.canVote()) {
                licznik++;
            }
        }

        return licznik;
    }

    // którzy obywatele mogą głosować
    public Citizen[] whoCanVote() {
        Citizen[] zdolniDoGlosowania = new Citizen[howManyCanVote()];

        int pozycjaNaKtoraWstawiam = 0;
        for (Citizen citizen : obywatele) {
            if (citizen.canVote()) {
                zdolniDoGlosowania[pozycjaNaKtoraWstawiam++] = citizen;
            }
        }

        return zdolniDoGlosowania;
    }
}
