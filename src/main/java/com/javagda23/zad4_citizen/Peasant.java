package com.javagda23.zad4_citizen;

public class Peasant extends Citizen {
    public Peasant(String imie) {
        super(imie);
    }

    @Override
    boolean canVote() {
        return false;
    }
}
