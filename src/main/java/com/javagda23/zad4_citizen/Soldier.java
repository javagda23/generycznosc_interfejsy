package com.javagda23.zad4_citizen;

public class Soldier extends Citizen {
    public Soldier(String imie) {
        super(imie);
    }

    @Override
    boolean canVote() {
        return true;
    }
}
