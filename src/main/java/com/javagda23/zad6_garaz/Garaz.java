package com.javagda23.zad6_garaz;

import java.util.Optional;

public class Garaz<T extends Samochod> {
    private T samochodMiejscePierwsze;
    private T samochodMiejsceDrugie;

    public Garaz() {
    }

    public void zaparkuj(T auto) {
        if (samochodMiejscePierwsze == null) {
            System.out.println("Zaparkowany, miejsce 1");
            samochodMiejscePierwsze = auto;
        } else if (samochodMiejsceDrugie == null) {
            System.out.println("Zaparkowany, miejsce 2");
            samochodMiejsceDrugie = auto;
        } else {
            System.out.println("Brak miejsca w garażu.");
        }
    }

    public Optional<T> wyprowadz(T auto) {
        if (samochodMiejscePierwsze == auto) {
            System.out.println("Zwracam samochód z miejsca 1.");
            samochodMiejscePierwsze = null;
            return Optional.of(auto);
        }
        if (samochodMiejsceDrugie == auto) {
            System.out.println("Zwracam samochód z miejsca 2.");
            samochodMiejsceDrugie = null;
            return Optional.of(auto);
        }
        return Optional.empty();
    }
}
