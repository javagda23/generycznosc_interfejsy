package com.javagda23.zad6_garaz;

public class Main {
    public static void main(String[] args) {
        Garaz<Samochod> samochodGaraz = new Garaz<>();

        Porshe p1 = new Porshe("XYZ", "Czerwony");
        Porshe p2 = new Porshe("ABC", "Czarny");
        BMW b1 = new BMW("DEF", "Zielony");
        BMW b2 = new BMW("GHA", "Fioletowy");

        samochodGaraz.zaparkuj(p1);
        samochodGaraz.zaparkuj(b1);

        System.out.println(samochodGaraz.wyprowadz(b2));
        System.out.println();
        System.out.println(samochodGaraz.wyprowadz(b1));
    }
}
