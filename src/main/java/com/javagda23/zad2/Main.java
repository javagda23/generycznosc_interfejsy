package com.javagda23.zad2;

public class Main {
    public static void main(String[] args) {
        Instrumentalny[] instrumentalnies = new Instrumentalny[3];
        instrumentalnies[0] = new Bęben();
        instrumentalnies[1] = new Gitara();
        instrumentalnies[2] = new Pianino();

        for (Instrumentalny instrumentalny : instrumentalnies) {
            instrumentalny.graj();
        }
    }
}
