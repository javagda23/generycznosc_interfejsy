package com.javagda23.zad3;

public class Main {
    public static void main(String[] args) {
        Jedzacy[] jedzacies = new Jedzacy[2];
        jedzacies[0] = new Programista();
        jedzacies[1] = new Weganin();

        Pokarm[] pokarms = new Pokarm[10]; // przygotowujemy pokarmy
        pokarms[0] = new Pokarm("a", TypPokarmu.OWOCE, 200);
        pokarms[1] = new Pokarm("b", TypPokarmu.MIESO, 300);
        pokarms[2] = new Pokarm("c", TypPokarmu.PIECZYWO, 400);
        pokarms[3] = new Pokarm("d", TypPokarmu.NABIAL, 250);
        pokarms[4] = new Pokarm("e", TypPokarmu.OWOCE, 210);
        pokarms[5] = new Pokarm("f", TypPokarmu.MIESO, 230);
        pokarms[6] = new Pokarm("g", TypPokarmu.ALKOHOL, 320);
        pokarms[7] = new Pokarm("h", TypPokarmu.PIECZYWO, 520);
        pokarms[8] = new Pokarm("i", TypPokarmu.MIESO, 100);
        pokarms[9] = new Pokarm("j", TypPokarmu.OWOCE, 50);

        for (Pokarm pokarm : pokarms) { // pętla "przez pokarmy"
            for (Jedzacy jedzacy : jedzacies) { // pętla przez jedzących
                jedzacy.jedz(pokarm); // faszerujemy instancje
            }
        }
        // powyższa pętla może być przeczytana w następujący sposób:
        // każdy pokarm z tablicy pokarms przekaż kolejno każdemu jedzącemu. (wszyscy jedzą wszystko)

        // wypisujemy rezultat "konkursu"
        for (Jedzacy jedzacy : jedzacies) {
            System.out.println(jedzacy.getClass().getSimpleName() + " zjadł: "
                    + jedzacy.ileGramowZjedzone() + " gramow i :"
                    + jedzacy.ilePosilkowZjedzone() + " posiłków");
        }
    }
}
