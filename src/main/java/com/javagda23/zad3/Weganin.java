package com.javagda23.zad3;

public class Weganin implements Jedzacy {
    private int iloscPosilkowZjedzonych = 0;
    private int iloscGramowZjedzonych = 0;

    @Override
    public void jedz(Pokarm pokarm) {
        if (pokarm.getTypPokarmu() == TypPokarmu.MIESO) {
            System.out.println("tfu");
            return;
        }

        iloscPosilkowZjedzonych++;
        iloscGramowZjedzonych += pokarm.getWaga();
    }

    @Override
    public int ilePosilkowZjedzone() {
        return iloscPosilkowZjedzonych;
    }

    @Override
    public int ileGramowZjedzone() {
        return iloscGramowZjedzonych;
    }
}
