package com.javagda23.przyklad_generycznosc;

import java.util.Arrays;

public class MainPerson {
    public static void main(String[] args) {
        Person[] persony = new Person[]{
          new Person("a", 5),
          new Person("b", 3),
          new Person("c", 20),
          new Person("d", 1),
          new Person("e", 13),
          new Person("f", 18),
          new Person("h", 300),
        };
        System.out.println(Arrays.toString(persony));
        Arrays.sort(persony);
        System.out.println(Arrays.toString(persony));

    }
}
