package com.javagda23.przyklad_generycznosc;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Integer[] tablica = new Integer[]{5, 23, 1, 22, 6, 89, 34, 2};
        System.out.println(Arrays.toString(tablica));

        Arrays.sort(tablica);

        System.out.println(Arrays.toString(tablica));

    }
}
